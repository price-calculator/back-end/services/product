namespace Product.Data.Repositories;

using System.Linq;
using System.Threading.Tasks;

using Context;

using Core.Domain.Models;

using Domain.Models.MaterialAggregate;

using Microsoft.EntityFrameworkCore;

using Specifications;

public class MaterialRepository : IMaterialRepository
{
    private readonly ProductContext _context;
    public IUnitOfWork UnitOfWork => _context;

    public MaterialRepository(ProductContext context) => _context = context;

    public MaterialEntity Add(MaterialEntity material) => _context.Materials.Add(material).Entity;

    public MaterialEntity Update(MaterialEntity material) => _context.Materials.Update(material).Entity;

    public MaterialEntity Delete(MaterialEntity material) => _context.Materials.Remove(material).Entity;

    public async Task<MaterialEntity> FindMaterialAsync(ISpecification<MaterialEntity> spec)
    {
        var query = ApplySpecification(spec);
        var material = await query.FirstOrDefaultAsync();

        if (material != null)
        {
            // Forces to get the entity from the db
            await _context.Entry<MaterialEntity>(material).ReloadAsync();
        }

        return material;
    }

    private IQueryable<MaterialEntity> ApplySpecification(ISpecification<MaterialEntity> spec) =>
        SpecificationEvaluator<MaterialEntity>.GetQuery(_context.Materials.AsQueryable(), spec);
}
