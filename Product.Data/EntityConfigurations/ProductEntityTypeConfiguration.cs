namespace Product.Data.EntityConfigurations;

using Domain.Models.ProductAggregate;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class ProductEntityTypeConfiguration : IEntityTypeConfiguration<ProductEntity>
{
    public void Configure(EntityTypeBuilder<ProductEntity> builder)
    {
        builder.ToTable("products");

        builder.HasKey(p => p.Id);
        builder.Property<int>(p => p.Id)
            .IsRequired();

        builder.Property<string>(p => p.Name)
            .HasMaxLength(256)
            .IsRequired();

        builder.Property<decimal>(p => p.LaborValue)
            .HasPrecision(10, 2)
            .IsRequired();

        builder.Property<string>(p => p.UserName)
            .HasMaxLength(256)
            .IsRequired();

        builder.Property<long>(o => o.Version)
            .HasDefaultValue(0)
            .IsConcurrencyToken(true);

        builder.HasIndex(p => new { p.UserName, p.Name }).IsUnique();
    }
}
