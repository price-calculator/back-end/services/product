namespace Product.Data.EntityConfigurations;

using Domain.Models.ProductAggregate;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class ProductMaterialTypeConfiguration : IEntityTypeConfiguration<ProductMaterial>
{
    public void Configure(EntityTypeBuilder<ProductMaterial> builder)
    {
        builder.ToTable("productmaterials");

        builder.HasKey(pm => new { pm.ProductId, pm.MaterialId });
    }
}
