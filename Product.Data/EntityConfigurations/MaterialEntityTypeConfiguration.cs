namespace Product.Data.EntityConfigurations;

using Domain.Models.MaterialAggregate;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class MaterialEntityTypeConfiguration : IEntityTypeConfiguration<MaterialEntity>
{
    public void Configure(EntityTypeBuilder<MaterialEntity> builder)
    {
        builder.ToTable("materials");

        builder.HasKey(m => m.Id);
        builder.Property<int>(m => m.Id)
            .ValueGeneratedNever()
            .IsRequired();

        builder.Property<string>(m => m.Name)
            .HasMaxLength(256)
            .IsRequired();

        builder.Property<string>(m => m.MeasurementName)
            .HasMaxLength(256)
            .IsRequired();

        builder.Property<decimal>(p => p.PricePerMeasurementUnit)
            .HasPrecision(10, 2)
            .IsRequired();

        builder.Property<string>(p => p.UserName)
            .HasMaxLength(256)
            .IsRequired();

        builder.Property<long>(m => m.Version).IsConcurrencyToken();

        builder.HasIndex(p => new { p.UserName, p.Name }).IsUnique();
    }
}
