namespace Product.ComponentTests.Handlers.Queries;
using System.Threading.Tasks;

using NUnit.Framework;

using Product.Application.Exceptions;
using Product.Application.Handlers.Queries;
using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

using Shouldly;

using static Product.ComponentTests.Constants;
using static Product.ComponentTests.Testing;

[TestFixture]
public class GetByIdQueryHandlerTests : TestBase
{
    [Test]
    public async Task Handler_HappyFlow_UpdatesProduct()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var letter = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        letter.UpsertMaterial(1, 1);
        letter.UpsertMaterial(2, 2);
        letter.UpsertMaterial(3, 1);
        var created = await AddProductAsync(letter);

        var query = new GetByIdQuery
        {
            Id = created.Id,
        };

        // Act
        var found = await SendAsync(query);

        // Assert
        found.ShouldNotBeNull();
        found.Name.ShouldBe(created.Name);
        found.LaborValue.ShouldBe(created.LaborValue);
        found.ProductMaterialLinks.ShouldNotBeNull();
        found.ProductMaterialLinks.Count.ShouldBe(3);
        foreach (var link in found.ProductMaterialLinks)
        {
            link.MaterialId.ShouldBeOneOf(1, 2, 3);
        }
    }



    [Test]
    public async Task Handler_ProductDoesNotExists_ThrowsException()
    {
        // Arrange
        var query = new GetByIdQuery
        {
            Id = 1,
        };

        // Act + Assert
        await Should.ThrowAsync<ProductException>(() => SendAsync(query));
    }
}
