namespace Product.ComponentTests.Handlers.IntegrationEvents;
using System;
using System.Threading;

using Common.Events.Models;

using NUnit.Framework;

using Product.Application.Specifications;
using Product.Domain.Models.MaterialAggregate;

using Shouldly;

using static Product.ComponentTests.Constants;
using static Product.ComponentTests.Testing;

[TestFixture]
public class MaterialCreatedIntegrationEventHandlerTests : TestBase
{
    [Test]
    public void Handle_HappyFlow_CreatesMaterial()
    {
        var @event = new MaterialCreatedIntegrationEvent(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);

        PublishIntegrationEvent(@event);

        var materialEntity = Execute(
            () =>
            {
                MaterialEntity found = null;
                do
                {
                    Thread.Sleep(200);
                    found = FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(@event.MaterialId, @event.UserName)).Result;
                } while (found == null);

                return found;
            },
            TimeSpan.FromSeconds(3),
            () => null);

        materialEntity.ShouldNotBeNull();
        materialEntity.Id.ShouldBe(@event.MaterialId);
        materialEntity.Name.ShouldBe(@event.Name);
        materialEntity.MeasurementName.ShouldBe(@event.MeasurementName);
        materialEntity.PricePerMeasurementUnit.ShouldBe(@event.PricePerMeasurementUnit);
        materialEntity.UserName.ShouldBe(@event.UserName);
        materialEntity.Version.ShouldBe(@event.Version);
    }
}
