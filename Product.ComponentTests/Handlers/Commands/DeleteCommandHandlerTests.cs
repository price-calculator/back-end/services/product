namespace Product.ComponentTests.Handlers.Commands;

using System.Threading.Tasks;

using MediatR;

using NUnit.Framework;

using Product.Application.Exceptions;
using Product.Application.Handlers.Commands;
using Product.Application.Specifications;
using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

using Shouldly;

using static Constants;
using static Testing;

[TestFixture]
public class DeleteCommandHandlerTests : TestBase
{
    [Test]
    public async Task Handler_HappyFlow_DeletesProduct()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var letter = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        letter.UpsertMaterial(1, 1);
        letter.UpsertMaterial(2, 2);
        letter.UpsertMaterial(3, 1);
        var created = await AddProductAsync(letter);

        var deleteCommand = new DeleteCommand
        {
            Id = created.Id,
        };

        // Act
        var result = await SendAsync(deleteCommand);

        // Assert
        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(created.Id, USER_USERNAME));
        result.ShouldBe(Unit.Value);
        found.ShouldBeNull();

    }

    [Test]
    public async Task Handler_ProductDoesNotExists_ThrowsException()
    {
        // Arrange
        var deleteCommand = new DeleteCommand
        {
            Id = 1,
        };

        // Act + Assert
        await Should.ThrowAsync<ProductException>(() => SendAsync(deleteCommand));
    }
}
