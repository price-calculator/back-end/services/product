namespace Product.ComponentTests.Handlers.Commands;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoFixture;

using NUnit.Framework;

using Product.Application.Exceptions;
using Product.Application.Handlers.Commands;
using Product.ComponentTests.Autofixture.Extensions;
using Product.Domain.Models.MaterialAggregate;

using Shouldly;

using static Product.ComponentTests.Constants;
using static Product.ComponentTests.Testing;

[TestFixture]
public class CreateCommandHandlerTests : TestBase
{
    private readonly Fixture _fixture = new();

    [Test]
    public async Task Handler_HappyFlow_CreatesProduct()
    {
        // Arrange
        //var envelope = _fixture.CreateWithFrozenConstructor<MaterialEntity>(new { id = 1, userName = USER_USERNAME, version = 0 });

        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var createCommand = new CreateCommand
        {
            Name = "Letter",
            LaborValue = 0.50M,
            Materials = new MaterialComposition[]
            {
                new MaterialComposition{ Id = 1, Quantity = 1},
                new MaterialComposition{ Id = 2, Quantity = 2},
                new MaterialComposition{ Id = 3, Quantity = 1},
            }
        };

        // Act
        var created = await SendAsync(createCommand);

        // Assert
        created.ShouldNotBeNull();
        created.Name.ShouldBe(createCommand.Name);
        created.LaborValue.ShouldBe(createCommand.LaborValue);
        created.ProductMaterialLinks.ShouldNotBeNull();
        created.ProductMaterialLinks.Count.ShouldBe(3);
        foreach (var link in created.ProductMaterialLinks)
        {
            link.MaterialId.ShouldBeOneOf(1, 2, 3);
        }
    }

    [Test]
    public async Task Handler_ProductAlreadyExists_ThrowsException()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var createCommand = new CreateCommand
        {
            Name = "Letter",
            LaborValue = 0.50M,
            Materials = new MaterialComposition[]
            {
                new MaterialComposition{ Id = 1, Quantity = 1},
                new MaterialComposition{ Id = 2, Quantity = 2},
                new MaterialComposition{ Id = 3, Quantity = 1},
            }
        };
        await SendAsync(createCommand);

        // Act + Assert
        await Should.ThrowAsync<ProductException>(() => SendAsync(createCommand));
    }
}
