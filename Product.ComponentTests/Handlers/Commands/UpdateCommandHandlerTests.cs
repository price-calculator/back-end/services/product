namespace Product.ComponentTests.Handlers.Commands;

using System.Threading.Tasks;

using NUnit.Framework;

using Product.Application.Exceptions;
using Product.Application.Handlers.Commands;
using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

using Shouldly;

using static Constants;
using static Testing;

[TestFixture]
public class UpdateCommandHandlerTests : TestBase
{
    [Test]
    public async Task Handler_HappyFlow_UpdatesProduct()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var letter = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        letter.UpsertMaterial(1, 1);
        letter.UpsertMaterial(2, 2);
        letter.UpsertMaterial(3, 1);
        await AddProductAsync(letter);

        var updateCommand = new UpdateCommand
        {
            Name = "Letter",
            LaborValue = 2.50M,
            Materials = new MaterialComposition[]
            {
                new MaterialComposition{ Id = 1, Quantity = 1},
                new MaterialComposition{ Id = 2, Quantity = 2},
            }
        };

        // Act
        var updated = await SendAsync(updateCommand);

        // Assert
        updated.ShouldNotBeNull();
        updated.Name.ShouldBe(updateCommand.Name);
        updated.LaborValue.ShouldBe(updateCommand.LaborValue);
        updated.UserName.ShouldBe(USER_USERNAME);
        updated.Version.ShouldBe(1);
        updated.ProductMaterialLinks.ShouldNotBeNull();
        updated.ProductMaterialLinks.Count.ShouldBe(2);
        foreach (var link in updated.ProductMaterialLinks)
        {
            link.MaterialId.ShouldBeOneOf(1, 2);
            link.MaterialId.ShouldNotBeOneOf(3);
        }

    }

    [Test]
    public async Task Handler_ProductDoesNotExists_ThrowsException()
    {
        // Arrange
        var updateCommand = new UpdateCommand
        {
            Name = "Letter",
            LaborValue = 2.50M,
            Materials = new MaterialComposition[]
            {
                new MaterialComposition{ Id = 1, Quantity = 1},
                new MaterialComposition{ Id = 2, Quantity = 2},
            }
        };

        // Act + Assert
        await Should.ThrowAsync<ProductException>(() => SendAsync(updateCommand));
    }
}
