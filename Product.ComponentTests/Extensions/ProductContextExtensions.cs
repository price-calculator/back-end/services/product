namespace Product.ComponentTests.Extensions;

using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Polly;
using Polly.Retry;

using Product.Data.Context;

public static class ProductContextExtensions
{
    public static async Task ApplyMigrationsAsync(this ProductContext context)
    {
        var policy = RetryPolicy
            .Handle<SqlException>()
            .WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
            {
            });

        await policy.Execute(async () =>
        {
            await context.Database.MigrateAsync().ConfigureAwait(false);
        });
    }
}
