namespace Product.ComponentTests.Extensions;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Moq;

using Product.Application.UserAccessors;

using static Product.ComponentTests.Constants;


public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddWebHostEnvironment(this IServiceCollection services)
    {
        var webHostEnvironmentMock = new Mock<IWebHostEnvironment>();
        webHostEnvironmentMock.Setup(w => w.EnvironmentName).Returns("Development");
        webHostEnvironmentMock.Setup(w => w.ApplicationName).Returns("Product.API");
        services.AddSingleton(webHostEnvironmentMock.Object);

        return services;
    }

    public static IServiceCollection AddMockedCustomServices(this IServiceCollection services)
    {
        var userAccessorMock = new Mock<IUserAccessor>();
        userAccessorMock.Setup(o => o.GetCurrentUsername()).Returns(USER_USERNAME);
        services.AddScoped(_ => userAccessorMock.Object);

        return services;
    }
}
