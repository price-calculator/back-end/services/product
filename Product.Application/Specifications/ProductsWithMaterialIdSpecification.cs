namespace Product.Application.Specifications;

using System.Linq;

using Core.Domain.Models;

using Domain.Models.ProductAggregate;

public class ProductsWithMaterialIdSpecification : BaseSpecification<ProductEntity>
{
    public ProductsWithMaterialIdSpecification(int materialId)
        : base(p => p.ProductMaterialLinks.Any(pm => pm.MaterialId == materialId)) =>
            AddInclude(p => p.ProductMaterialLinks);
}
