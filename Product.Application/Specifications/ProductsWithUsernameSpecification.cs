namespace Product.Application.Specifications;

using Core.Domain.Models;

using Domain.Models.ProductAggregate;

public class ProductsWithUsernameSpecification : BaseSpecification<ProductEntity>
{
    public ProductsWithUsernameSpecification(string username, int skip = 0, int take = 20) : base(p => p.UserName == username)
    {
        AddInclude(p => p.ProductMaterialLinks);
        AddInclude($"{nameof(ProductEntity.ProductMaterialLinks)}.{nameof(ProductMaterial.Material)}");
        Skip = skip;
        Take = take;
    }
}
