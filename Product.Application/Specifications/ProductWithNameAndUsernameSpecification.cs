namespace Product.Application.Specifications;

using Core.Domain.Models;

using Domain.Models.ProductAggregate;

public class ProductWithNameAndUsernameSpecification : BaseSpecification<ProductEntity>
{
    public ProductWithNameAndUsernameSpecification(string name, string username)
        : base(p => p.Name == name && p.UserName == username) =>
            AddInclude(p => p.ProductMaterialLinks);
}
