namespace Product.Application.Handlers.Commands;

using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Domain.Models.ProductAggregate;

using Exceptions;

using MediatR;

using Specifications;

using UserAccessors;

public class UpdateCommandHandler : IRequestHandler<UpdateCommand, ProductEntity>
{
    private readonly IProductRepository _repository;
    private readonly IUserAccessor _userAccessor;

    public UpdateCommandHandler(IProductRepository productRepository, IUserAccessor userAccessor)
    {
        _repository = productRepository;
        _userAccessor = userAccessor;
    }

    public async Task<ProductEntity> Handle(UpdateCommand request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        var product = await _repository.FindProductAsync(new ProductWithNameAndUsernameSpecification(request.Name, username));

        if (product == null)
        {
            throw new ProductException(HttpStatusCode.NotFound, new { message = $"Product {request.Name} not found." });
        }

        product.LaborValue = request.LaborValue;

        // Remove materials
        foreach (var productMaterial in product.ProductMaterialLinks.ToList())
        {
            product.RemoveMaterial(productMaterial.MaterialId);
        }

        // Update or Add items
        foreach (var material in request.Materials)
        {
            product.UpsertMaterial(material.Id, material.Quantity);
        }
        
        var updated = _repository.Update(product);

        var result = await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

        if (result)
        {
            return updated;
        }

        throw new Exception("Problem saving changes");
    }
}
