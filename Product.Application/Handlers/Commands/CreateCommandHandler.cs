namespace Product.Application.Handlers.Commands;

using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Domain.Models.ProductAggregate;

using Exceptions;

using MediatR;

using Specifications;

using UserAccessors;

public class CreateCommandHandler : IRequestHandler<CreateCommand, ProductEntity>
{
    private readonly IProductRepository _repository;
    private readonly IUserAccessor _userAccessor;

    public CreateCommandHandler(IProductRepository productRepository, IUserAccessor userAccessor)
    {
        _repository = productRepository;
        _userAccessor = userAccessor;
    }

    public async Task<ProductEntity> Handle(CreateCommand request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        if (await _repository.FindProductAsync(new ProductWithNameAndUsernameSpecification(request.Name, username)) != null)
        {
            throw new ProductException(HttpStatusCode.BadRequest, new { message = $"The product {request.Name} already exists." });
        }

        var productEntity = new ProductEntity(request.Name, request.LaborValue, username);

        foreach (var material in request.Materials)
        {
            productEntity.UpsertMaterial(material.Id, material.Quantity);
        }

        var added = _repository.Add(productEntity);

        var result = await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        if (result)
        {
            return added;
        }

        throw new Exception("Problem saving changes");
    }
}
