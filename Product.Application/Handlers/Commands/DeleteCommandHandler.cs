namespace Product.Application.Handlers.Commands;

using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Domain.Models.ProductAggregate;

using Exceptions;

using MediatR;

using Specifications;

using UserAccessors;

public class DeleteCommandHandler : IRequestHandler<DeleteCommand, Unit>
{
    private readonly IProductRepository _repository;
    private readonly IUserAccessor _userAccessor;

    public DeleteCommandHandler(IProductRepository repository, IUserAccessor userAccessor)
    {
        _repository = repository;
        _userAccessor = userAccessor;
    }

    public async Task<Unit> Handle(DeleteCommand request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        var product = await _repository.FindProductAsync(new ProductWithIdAndUsernameSpecification(request.Id, username));
        if (product == null)
        {
            throw new ProductException(HttpStatusCode.NotFound, new { message = $"Material not found." });
        }

        _repository.Delete(product);
        var result = await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

        if (result)
        {
            return Unit.Value;
        }

        throw new Exception("Problem saving changes");
    }
}
