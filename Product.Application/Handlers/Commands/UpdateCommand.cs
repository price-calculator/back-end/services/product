namespace Product.Application.Handlers.Commands;

using Domain.Models.ProductAggregate;

using MediatR;

public class UpdateCommand : IRequest<ProductEntity>
{
    public string Name { get; set; }
    public decimal LaborValue { get; set; }
    public MaterialComposition[] Materials { get; set; }
}
