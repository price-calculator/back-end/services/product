namespace Product.Application.Handlers.Queries;

using Application.Models;

using MediatR;

public class GetAllQuery : IRequest<GetAllQueryResponse>
{
    public PaginationParams PaginationParams { get; set; }

    public GetAllQuery(PaginationParams paginationParams)
    {
        PaginationParams = paginationParams;
    }
}
