namespace Product.Application.Handlers.Queries;

using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Domain.Models.ProductAggregate;

using Exceptions;

using MediatR;

using Specifications;

using UserAccessors;

public class GetByIdQueryHandler : IRequestHandler<GetByIdQuery, ProductEntity>
{
    private readonly IProductRepository _repository;
    private readonly IUserAccessor _userAccessor;

    public GetByIdQueryHandler(IProductRepository repository, IUserAccessor userAccessor)
    {
        _repository = repository;
        _userAccessor = userAccessor;
    }

    public async Task<ProductEntity> Handle(GetByIdQuery request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        var product = await _repository.FindProductAsync(new ProductWithIdAndUsernameSpecification(request.Id, username));
        if (product == null)
        {
            throw new ProductException(HttpStatusCode.NotFound, new { message = "Product not found." });
        }

        return product;
    }
}
