namespace Product.Application.Models;

using System.Linq;

using Domain.Models.ProductAggregate;

public class ProductDetailDTO
{
    public int Id { get; }
    public string Name { get; }
    public decimal LaborValue { get; set; }
    public MaterialDetailDTO[] Materials;
    public string UserName { get; }
    public long Version { get; }

    public ProductDetailDTO(int id, string name, decimal laborValue, MaterialDetailDTO[] materials, string userName, long version)
    {
        Id = id;
        Name = name;
        Materials = materials;
        LaborValue = laborValue;
        UserName = userName;
        Version = version;
    }

    public static ProductDetailDTO MapFromProductEntity(ProductEntity product)
    {
        var materials = product.ProductMaterialLinks
                               .Select(pm => new MaterialDetailDTO(
                                   pm.Material.Id,
                                   pm.Material.Name,
                                   pm.Material.MeasurementName,
                                   pm.Material.PricePerMeasurementUnit))
                                .ToArray();

        return new ProductDetailDTO(
            product.Id,
            product.Name,
            product.LaborValue,
            materials,
            product.UserName,
            product.Version
        );
    }
}
