namespace Product.Application.Models;

using Domain.Models.MaterialAggregate;

public class MaterialDetailDTO
{
    public int Id { get; set; }
    public string Name { get; }
    public string MeasurementName { get; }
    public decimal PricePerMeasurementUnit { get; }

    public MaterialDetailDTO(int id, string name, string measurementName, decimal pricePerMeasurementUnit)
    {
        Id = id;
        Name = name;
        MeasurementName = measurementName;
        PricePerMeasurementUnit = pricePerMeasurementUnit;
    }

    public static MaterialDetailDTO MapFromMaterialEntity(MaterialEntity material) =>
        new(material.Id, material.Name, material.MeasurementName, material.PricePerMeasurementUnit);
}
