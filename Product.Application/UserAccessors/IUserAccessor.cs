namespace Product.Application.UserAccessors;

public interface IUserAccessor
{
    string GetCurrentUsername();
    string GetCurrentToken();
}
