namespace Product.Domain.Models.ProductAggregate;

using System.Collections.Generic;
using System.Linq;

using Core.Domain.Models;

public class ProductEntity : Entity, IAgreggateRoot
{
    public string Name { get; set; }
    public decimal LaborValue { get; set; }
    public string UserName { get; set; }
    public ICollection<ProductMaterial> ProductMaterialLinks { get; set; }

    public ProductEntity(string name, decimal laborValue, string userName)
    {
        Name = name;
        LaborValue = laborValue;
        UserName = userName;
        ProductMaterialLinks = new List<ProductMaterial>();
    }

    public void IncreaseVersion() => Version++;

    public void UpsertMaterial(int id, int quantity)
    {
        var found = ProductMaterialLinks.FirstOrDefault(pm => pm.MaterialId == id);
        if (found == null)
        {
            ProductMaterialLinks.Add(new ProductMaterial { Product = this, MaterialId = id, Quantity = quantity });
        }
        else
        {
            found.Quantity = quantity;
        }
    }

    public bool RemoveMaterial(int id)
    {
        var found = ProductMaterialLinks.FirstOrDefault(pm => pm.MaterialId == id);
        if (found != null)
        {
            ProductMaterialLinks.Remove(found);
            return true;
        }

        return false;
    }
}
