namespace Product.Domain.Models.MaterialAggregate;

using System.Collections.Generic;

using Core.Domain.Models;

using ProductAggregate;

public class MaterialEntity : Entity, IAgreggateRoot
{
    public string Name { get; set; }
    public string MeasurementName { get; set; }
    public decimal PricePerMeasurementUnit { get; set; }
    public string UserName { get; set; }
    public ICollection<ProductMaterial> ProductMaterialLinks { get; set; }

    public MaterialEntity(int id, string name, string measurementName, decimal pricePerMeasurementUnit, string userName, long version)
    {
        Id = id;
        Name = name;
        MeasurementName = measurementName;
        PricePerMeasurementUnit = pricePerMeasurementUnit;
        UserName = userName;
        Version = version;
    }

    public void SetVersion(long version)
    {
        if (Version != version - 1)
        {
            throw new System.Exception("ConcurrencyException");
        }

        Version = version;
    }
}
