namespace Product.UnitTests.Domain;

using NUnit.Framework;

using Product.Domain.Models.ProductAggregate;

using Shouldly;

[TestFixture]
public class ProductEntityTests
{
    [Test]
    public void ProductTest_IncreaseVersion_Success()
    {
        // Arrange
        var product = new ProductEntity("Letter", 0.50M, "John");

        // Act
        product.IncreaseVersion();

        // Assert
        product.ShouldSatisfyAllConditions(
            () => product.Name.ShouldBe("Letter"),
            () => product.LaborValue.ShouldBe(0.50M),
            () => product.UserName.ShouldBe("John"),
            () => product.ProductMaterialLinks.Count.ShouldBe(0),
            () => product.Version.ShouldBe(1),
            () => product.ProductMaterialLinks.ShouldBeEmpty()
        );
    }

    [Test]
    public void UpsertMaterial_OneMaterial_Success()
    {
        // Arrange
        var product = new ProductEntity("Letter", 0.50M, "John");

        // Act
        product.UpsertMaterial(1, 10);

        // Assert
        product.ShouldSatisfyAllConditions(
            () => product.Name.ShouldBe("Letter"),
            () => product.LaborValue.ShouldBe(0.50M),
            () => product.UserName.ShouldBe("John"),
            () => product.Version.ShouldBe(0),
            () => product.ProductMaterialLinks.ShouldNotBeNull(),
            () => product.ProductMaterialLinks.Count.ShouldBe(1),
            () => product.ProductMaterialLinks.ShouldContain(pm => pm.MaterialId == 1 && pm.Quantity == 10)
        );
    }


    [Test]
    public void UpsertMaterial_TwoDifferentMaterials_Success()
    {
        // Arrange
        var product = new ProductEntity("Letter", 0.50M, "John");

        // Act
        product.UpsertMaterial(1, 10);
        product.UpsertMaterial(2, 20);

        // Assert
        product.ShouldSatisfyAllConditions(
            () => product.Name.ShouldBe("Letter"),
            () => product.LaborValue.ShouldBe(0.50M),
            () => product.UserName.ShouldBe("John"),
            () => product.Version.ShouldBe(0),
            () => product.ProductMaterialLinks.ShouldNotBeNull(),
            () => product.ProductMaterialLinks.Count.ShouldBe(2),
            () => product.ProductMaterialLinks.ShouldContain(pm => pm.MaterialId == 1 && pm.Quantity == 10),
            () => product.ProductMaterialLinks.ShouldContain(pm => pm.MaterialId == 2 && pm.Quantity == 20)
        );
    }

    [Test]
    public void UpsertMaterial_TwoEqualMaterials_Success()
    {
        // Arrange
        var product = new ProductEntity("Letter", 0.50M, "John");

        // Act
        product.UpsertMaterial(1, 10);
        product.UpsertMaterial(1, 10);

        // Assert
        product.ShouldSatisfyAllConditions(
            () => product.Name.ShouldBe("Letter"),
            () => product.LaborValue.ShouldBe(0.50M),
            () => product.UserName.ShouldBe("John"),
            () => product.Version.ShouldBe(0),
            () => product.ProductMaterialLinks.ShouldNotBeNull(),
            () => product.ProductMaterialLinks.Count.ShouldBe(1),
            () => product.ProductMaterialLinks.ShouldContain(pm => pm.MaterialId == 1 && pm.Quantity == 10)
        );
    }

    [Test]
    public void UpsertMaterial_UpdateMaterial_Success()
    {
        // Arrange
        var product = new ProductEntity("Letter", 0.50M, "John");
        product.UpsertMaterial(1, 10);

        // Act
        product.UpsertMaterial(1, 20);

        // Assert
        product.ShouldSatisfyAllConditions(
            () => product.Name.ShouldBe("Letter"),
            () => product.LaborValue.ShouldBe(0.50M),
            () => product.UserName.ShouldBe("John"),
            () => product.Version.ShouldBe(0),
            () => product.ProductMaterialLinks.ShouldNotBeNull(),
            () => product.ProductMaterialLinks.Count.ShouldBe(1),
            () => product.ProductMaterialLinks.ShouldContain(pm => pm.MaterialId == 1 && pm.Quantity == 20)
        );
    }

    [Test]
    public void RemoveMaterial_ExistingMaterial_Success()
    {
        // Arrange
        var product = new ProductEntity("Letter", 0.50M, "John");
        product.UpsertMaterial(1, 10);

        // Act
        product.RemoveMaterial(1);

        // Assert
        product.ShouldSatisfyAllConditions(
            () => product.Name.ShouldBe("Letter"),
            () => product.LaborValue.ShouldBe(0.50M),
            () => product.UserName.ShouldBe("John"),
            () => product.Version.ShouldBe(0),
            () => product.ProductMaterialLinks.ShouldNotBeNull(),
            () => product.ProductMaterialLinks.Count.ShouldBe(0),
            () => product.ProductMaterialLinks.ShouldNotContain(pm => pm.MaterialId == 1 && pm.Quantity == 10)
        );
    }

    [Test]
    public void RemoveMaterial_NonExistingMaterial_Success()
    {
        // Arrange
        var product = new ProductEntity("Letter", 0.50M, "John");
        product.UpsertMaterial(1, 10);

        // Act
        product.RemoveMaterial(2);

        // Assert
        product.ShouldSatisfyAllConditions(
            () => product.Name.ShouldBe("Letter"),
            () => product.LaborValue.ShouldBe(0.50M),
            () => product.UserName.ShouldBe("John"),
            () => product.Version.ShouldBe(0),
            () => product.ProductMaterialLinks.ShouldNotBeNull(),
            () => product.ProductMaterialLinks.Count.ShouldBe(1),
            () => product.ProductMaterialLinks.ShouldContain(pm => pm.MaterialId == 1 && pm.Quantity == 10)
        );
    }
}
