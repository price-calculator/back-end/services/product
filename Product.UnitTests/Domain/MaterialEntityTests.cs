namespace Product.UnitTests.Domain;

using System;

using Core.Domain.Models;

using NUnit.Framework;

using Product.Domain.Models.MaterialAggregate;

using Shouldly;

[TestFixture]
public class MaterialEntityTests
{
    [Test]
    public void Material_SetVersion_Success()
    {
        // Arrange
        var material = new MaterialEntity(1, "Paper", "Units", 0.25M, "John", 0);

        // Act
        material.SetVersion(1);

        // Assert
        material.ShouldSatisfyAllConditions(
            () => material.Id.ShouldBe(1),
            () => material.Name.ShouldBe("Paper"),
            () => material.MeasurementName.ShouldBe("Units"),
            () => material.PricePerMeasurementUnit.ShouldBe(0.25M),
            () => material.UserName.ShouldBe("John"),
            () => material.Version.ShouldBe(1)
        );
    }

    [Test]
    public void Material_SetVersion_Fail()
    {
        // Arrange
        var material = new MaterialEntity(1, "Paper", "Units", 0.25M, "John", 1);

        // Act
        Should.Throw<Exception>(() => material.SetVersion(1));
    }
}
