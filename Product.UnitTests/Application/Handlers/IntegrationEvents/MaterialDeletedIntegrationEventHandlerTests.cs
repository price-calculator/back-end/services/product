namespace Product.UnitTests.Application.Handlers.IntegrationEvents;

using System;
using System.Threading.Tasks;

using Common.Events.Models;

using Core.Domain.Models;

using Microsoft.Extensions.Logging;

using Moq;

using NUnit.Framework;

using Product.Application.Handlers.IntegrationEvents;
using Product.Application.Specifications;
using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

using Shouldly;

[TestFixture]
public class MaterialDeletedIntegrationEventHandlerTests : HandlerFixture
{
    private readonly Mock<IUnitOfWork> _materialUnitOfWorkMock = new(MockBehavior.Strict);
    private readonly Mock<IUnitOfWork> _productUnitOfWorkMock = new(MockBehavior.Strict);
    private readonly Mock<IMaterialRepository> _materialRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IProductRepository> _productRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<ILogger<MaterialDeletedIntegrationEventHandler>> _loggerMock = new(MockBehavior.Loose);

    private MaterialDeletedIntegrationEventHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _loggerMock.Reset();
        _materialUnitOfWorkMock.Reset();
        _productUnitOfWorkMock.Reset();
        _materialRepositoryMock.Reset();
        _productRepositoryMock.Reset();
        _sut = new(_materialRepositoryMock.Object, _productRepositoryMock.Object, _loggerMock.Object);
    }

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        var materialRepositoryMockSequence = new MockSequence();
        var productRepositoryMockSequence = new MockSequence();

        _materialRepositoryMock.InSequence(materialRepositoryMockSequence)
                               .Setup(o => o.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>()))
                               .ReturnsAsync(materials[0]);

        _productRepositoryMock.InSequence(productRepositoryMockSequence)
                              .Setup(o => o.FindProductsAsync(It.IsAny<ProductsWithMaterialIdSpecification>()))
                              .ReturnsAsync(products);

        _productRepositoryMock.InSequence(productRepositoryMockSequence)
                              .Setup(x => x.Update(products[0]))
                              .Returns(products[0]);

        _productRepositoryMock.InSequence(productRepositoryMockSequence)
                              .Setup(x => x.Update(products[1]))
                              .Returns(products[1]);

        _materialRepositoryMock.InSequence(materialRepositoryMockSequence)
                               .Setup(x => x.Delete(It.Is<MaterialEntity>(x => x.Name == materials[0].Name)))
                               .Returns(materials[0]);

        _productRepositoryMock.InSequence(productRepositoryMockSequence)
                              .Setup(o => o.UnitOfWork)
                              .Returns(_productUnitOfWorkMock.Object);

        _materialRepositoryMock.InSequence(materialRepositoryMockSequence)
                               .Setup(o => o.UnitOfWork)
                               .Returns(_materialUnitOfWorkMock.Object);

        _productUnitOfWorkMock.InSequence(productRepositoryMockSequence)
                            .Setup(o => o.SaveEntitiesAsync(default))
                              .ReturnsAsync(true);

        _materialUnitOfWorkMock.InSequence(materialRepositoryMockSequence)
                               .Setup(o => o.SaveEntitiesAsync(default))
                               .ReturnsAsync(true);

        var materialDeletedIntegrationEvent = new MaterialDeletedIntegrationEvent(materials[0].Id, materials[0].Name, materials[0].UserName);

        // Act
        await _sut.Handle(materialDeletedIntegrationEvent);

        // Assert
        Assert.Pass();

        _productUnitOfWorkMock.VerifyAll();
        _materialUnitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialAlreadyExists_Fail()
    {
        // Arrange
        _materialRepositoryMock.Setup(o => o.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>()))
                               .ReturnsAsync((MaterialEntity)null);

        var materialDeletedIntegrationEvent = new MaterialDeletedIntegrationEvent(materials[0].Id, materials[0].Name, materials[0].UserName);

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(materialDeletedIntegrationEvent));

        _productUnitOfWorkMock.VerifyAll();
        _materialUnitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialProblemSavingChanges_Fail()
    {
        // Arrange
        var materialRepositoryMockSequence = new MockSequence();
        var productRepositoryMockSequence = new MockSequence();

        _materialRepositoryMock.InSequence(materialRepositoryMockSequence)
                               .Setup(o => o.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>()))
                               .ReturnsAsync(materials[0]);

        _productRepositoryMock.InSequence(productRepositoryMockSequence)
                              .Setup(o => o.FindProductsAsync(It.IsAny<ProductsWithMaterialIdSpecification>()))
                              .ReturnsAsync(products);

        _productRepositoryMock.InSequence(productRepositoryMockSequence)
                              .Setup(x => x.Update(products[0]))
                              .Returns(products[0]);

        _productRepositoryMock.InSequence(productRepositoryMockSequence)
                              .Setup(x => x.Update(products[1]))
                              .Returns(products[1]);

        _materialRepositoryMock.InSequence(materialRepositoryMockSequence)
                               .Setup(x => x.Delete(It.Is<MaterialEntity>(x => x.Name == materials[0].Name)))
                               .Returns(materials[0]);

        _productRepositoryMock.InSequence(productRepositoryMockSequence)
                              .Setup(o => o.UnitOfWork)
                              .Returns(_productUnitOfWorkMock.Object);

        _materialRepositoryMock.InSequence(materialRepositoryMockSequence)
                               .Setup(o => o.UnitOfWork)
                               .Returns(_materialUnitOfWorkMock.Object);

        _productUnitOfWorkMock.InSequence(productRepositoryMockSequence)
                              .Setup(o => o.SaveEntitiesAsync(default))
                              .ReturnsAsync(false);

        _materialUnitOfWorkMock.InSequence(productRepositoryMockSequence)
                               .Setup(o => o.SaveEntitiesAsync(default))
                               .ReturnsAsync(false);

        var materialDeletedIntegrationEvent = new MaterialDeletedIntegrationEvent(materials[0].Id, materials[0].Name, materials[0].UserName);

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(materialDeletedIntegrationEvent));

        _productUnitOfWorkMock.VerifyAll();
        _materialUnitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

}
