namespace Product.UnitTests.Application.Handlers;

using System.Collections.Generic;
using System.Linq;

using Core.Domain.Models;

using NUnit.Framework;

using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

[TestFixture]
public class HandlerFixture
{
    protected IList<ProductEntity> products = new List<ProductEntity>();
    protected IList<MaterialEntity> materials = new List<MaterialEntity>();
    protected const string USERNAME = "test";

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {

        var material = new MaterialEntity(1, "Envelope", "Units", 0.25M, USERNAME, 0);
        materials.Add(material);

        material = new MaterialEntity(2, "Paper", "Units", 0.10M, USERNAME, 0);
        materials.Add(material);

        var product = new ProductEntity("Letter", 1.00M, USERNAME);
        product.UpsertMaterial(1, 10);
        product.ProductMaterialLinks.ElementAt(0).Material = materials[0];
        product.UpsertMaterial(2, 20);
        product.ProductMaterialLinks.ElementAt(1).Material = materials[1];
        SetProductId(product, 1);
        products.Add(product);

        product = new ProductEntity("Maxi Letter", 5.00M, USERNAME);
        product.UpsertMaterial(1, 50);
        product.ProductMaterialLinks.ElementAt(0).Material = materials[0];
        product.UpsertMaterial(2, 100);
        product.ProductMaterialLinks.ElementAt(1).Material = materials[1];
        SetProductId(product, 2);
        products.Add(product);
    }

    private static void SetProductId(ProductEntity product, int id) =>
        typeof(Entity)
            .GetProperty("Id")
            .SetValue(product, id);
}
