namespace Product.UnitTests.Application.Handlers.Commands;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Core.Domain.Models;

using Moq;

using NUnit.Framework;

using Product.Application.Exceptions;
using Product.Application.Handlers.Commands;
using Product.Application.Specifications;
using Product.Application.UserAccessors;
using Product.Domain.Models.ProductAggregate;

using Shouldly;

[TestFixture]
public class UpdateCommandHandlerTests : HandlerFixture
{
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new(MockBehavior.Loose);
    private readonly Mock<IProductRepository> _productRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);

    private UpdateCommandHandler _sut;

    [SetUp]
    public void SetUp() => _sut = new UpdateCommandHandler(_productRepositoryMock.Object, _userAccessorMock.Object);

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(x => x.FindProductAsync(It.IsAny<ProductWithNameAndUsernameSpecification>())).ReturnsAsync(products[0]);

        _productRepositoryMock.Setup(x => x.Update(products[0])).Returns(products[0]);

        _productRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(true);

        var updateCommand = new UpdateCommand
        {
            Name = products[0].Name,
            LaborValue = products[0].LaborValue,
            Materials = products[0].ProductMaterialLinks
                .Select(x => new MaterialComposition()
                {
                    Id = x.MaterialId,
                    Quantity = x.Quantity
                }).ToArray()
        };

        // Act
        var updated = await _sut.Handle(updateCommand, default);

        // Assert
        updated.ShouldBe(products[0]);

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_ProductDoesNotExists_Fail()
    {
        // Arrange
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(x => x.FindProductAsync(It.IsAny<ProductWithNameAndUsernameSpecification>())).ReturnsAsync((ProductEntity)null);

        var updateCommand = new UpdateCommand
        {
            Name = products[0].Name,
            LaborValue = products[0].LaborValue,
            Materials = products[0].ProductMaterialLinks
                .Select(x => new MaterialComposition()
                {
                    Id = x.MaterialId,
                    Quantity = x.Quantity
                }).ToArray()
        };

        // Act + Assert
        await Should.ThrowAsync<ProductException>(() => _sut.Handle(updateCommand, default));

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_ProductProblemSavingChanges_Fail()
    {
        // Arrange
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(x => x.FindProductAsync(It.IsAny<ProductWithNameAndUsernameSpecification>())).ReturnsAsync(products[0]);

        _productRepositoryMock.Setup(x => x.Update(products[0])).Returns(products[0]);

        _productRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(false);

        var updateCommand = new UpdateCommand
        {
            Name = products[0].Name,
            LaborValue = products[0].LaborValue,
            Materials = products[0].ProductMaterialLinks
                .Select(x => new MaterialComposition()
                {
                    Id = x.MaterialId,
                    Quantity = x.Quantity
                }).ToArray()
        };

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(updateCommand, default));

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }
}
