namespace Product.UnitTests.Application.Handlers.Commands;

using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Core.Domain.Models;

using MediatR;

using Moq;

using NUnit.Framework;

using Product.Application.Exceptions;
using Product.Application.Handlers.Commands;
using Product.Application.Specifications;
using Product.Application.UserAccessors;
using Product.Domain.Models.ProductAggregate;

using Shouldly;

[TestFixture]
public class DeleteCommandHandlerTests : HandlerFixture
{
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new(MockBehavior.Loose);
    private readonly Mock<IProductRepository> _productRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);

    private DeleteCommandHandler _sut;

    [SetUp]
    public void SetUp() => _sut = new DeleteCommandHandler(_productRepositoryMock.Object, _userAccessorMock.Object);

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(x => x.FindProductAsync(It.IsAny<ProductWithIdAndUsernameSpecification>())).ReturnsAsync(products[0]);

        _productRepositoryMock.Setup(x => x.Delete(products[0])).Returns(products[0]);

        _productRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(true);

        var deleteCommand = new DeleteCommand()
        {
            Id = products[0].Id
        };

        // Act
        var result = await _sut.Handle(deleteCommand, default);

        // Assert
        result.ShouldBe(Unit.Value);

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_ProductDoesNotExists_Fail()
    {
        // Arrange
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(x => x.FindProductAsync(It.IsAny<ProductWithIdAndUsernameSpecification>())).ReturnsAsync((ProductEntity)null);

        var deleteCommand = new DeleteCommand()
        {
            Id = products[0].Id
        };

        // Act + Assert
        await Should.ThrowAsync<ProductException>(() => _sut.Handle(deleteCommand, default));

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_ProductProblemSavingChanges_Fail()
    {
        // Arrange
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(x => x.FindProductAsync(It.IsAny<ProductWithIdAndUsernameSpecification>())).ReturnsAsync(products[0]);

        _productRepositoryMock.Setup(x => x.Delete(products[0])).Returns(products[0]);

        _productRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(false);

        var deleteCommand = new DeleteCommand()
        {
            Id = products[0].Id
        };

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(deleteCommand, default));

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }
}
