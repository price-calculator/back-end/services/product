namespace Product.UnitTests.Application.Handlers.Queries;

using System;
using System.Threading.Tasks;

using Moq;

using NUnit.Framework;

using Product.Application.Handlers.Queries;
using Product.Application.Models;
using Product.Application.Specifications;
using Product.Application.UserAccessors;
using Product.Domain.Models.ProductAggregate;

using Shouldly;

[TestFixture]
public class GetAllQueryHandlerTests : HandlerFixture
{
    private readonly Mock<IProductRepository> _productRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);

    private GetAllQueryHandler _sut;

    [SetUp]
    public void SetUp() => _sut = new GetAllQueryHandler(_productRepositoryMock.Object, _userAccessorMock.Object);

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(x => x.CountProductsAsync(USERNAME)).ReturnsAsync(6);

        _productRepositoryMock.Setup(x => x.FindProductsAsync(It.IsAny<ProductsWithUsernameSpecification>())).ReturnsAsync(products);

        var query = new GetAllQuery(new PaginationParams(0, 3));

        // Act
        var response = await _sut.Handle(query, default);

        response.ShouldNotBeNull();
        response.Total.ShouldBe(6);
        response.Products.ShouldNotBeEmpty();
        foreach (var product in products)
        {
            response.Products.ShouldContain(p => p.Id == product.Id);
        }

        _userAccessorMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_ProductsDoesNotExists_Fail()
    {
        // Arrange
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(x => x.CountProductsAsync(USERNAME)).ReturnsAsync(0);

        _productRepositoryMock.Setup(x => x.FindProductsAsync(It.IsAny<ProductsWithUsernameSpecification>())).ReturnsAsync(Array.Empty<ProductEntity>());

        var query = new GetAllQuery(new PaginationParams(0, 3));

        // Act
        var found = await _sut.Handle(query, default);

        // Assert
        found.ShouldNotBeNull();
        found.Products.ShouldBeEmpty();
        found.Total.ShouldBe(0);

        _userAccessorMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }
}
