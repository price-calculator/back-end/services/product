namespace Product.UnitTests.Application.Handlers.Queries;

using System.Threading.Tasks;

using Moq;

using NUnit.Framework;

using Product.Application.Exceptions;
using Product.Application.Handlers.Queries;
using Product.Application.Specifications;
using Product.Application.UserAccessors;
using Product.Domain.Models.ProductAggregate;

using Shouldly;

[TestFixture]
public class GetByIdQueryHandlerTests : HandlerFixture
{
    private readonly Mock<IProductRepository> _productRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);

    private GetByIdQueryHandler _sut;

    [SetUp]
    public void SetUp() => _sut = new GetByIdQueryHandler(_productRepositoryMock.Object, _userAccessorMock.Object);

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange        
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(o => o.FindProductAsync(It.IsAny<ProductWithIdAndUsernameSpecification>())).ReturnsAsync(products[0]);

        var getByIdQuery = new GetByIdQuery
        {
            Id = products[0].Id,
        };

        // Act
        var found = await _sut.Handle(getByIdQuery, default);

        // Assert
        found.ShouldBe(products[0]);

        _userAccessorMock.VerifyAll();
        _productRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialDoesNotExists_Fail()
    {
        // Arrange       
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(USERNAME);

        _productRepositoryMock.Setup(o => o.FindProductAsync(It.IsAny<ProductWithIdAndUsernameSpecification>())).ReturnsAsync((ProductEntity)null);

        var getByIdQuery = new GetByIdQuery
        {
            Id = products[0].Id,
        };

        // Act + Assert
        await Should.ThrowAsync<ProductException>(() => _sut.Handle(getByIdQuery, default));
    }
}
