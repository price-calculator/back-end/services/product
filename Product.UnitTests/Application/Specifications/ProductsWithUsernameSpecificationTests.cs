namespace Product.UnitTests.Application.Specifications;

using System.Linq;

using NUnit.Framework;

using Product.Application.Specifications;

using Shouldly;

[TestFixture]
public class ProductsWithUsernameSpecificationTests : SpecificationFixture
{
    [Test]
    public void Evaluate_GetAllProducts_Success()
    {
        // Arrange
        var firstUserProducts = products.Where(x => x.UserName == FIRST_USERNAME);

        // Act
        var founds = products.Where(new ProductsWithUsernameSpecification(FIRST_USERNAME, 0, 2).Criteria.Compile());

        // Assert
        founds.ShouldNotBeEmpty();
        founds.Count().ShouldBe(2);

        foreach (var product in founds)
        {
            firstUserProducts.ShouldContain(product);
        }
    }
}
