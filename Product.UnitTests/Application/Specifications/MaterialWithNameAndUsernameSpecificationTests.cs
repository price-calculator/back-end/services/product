namespace Product.UnitTests.Application.Specifications;

using System.Linq;

using NUnit.Framework;

using Product.Application.Specifications;

using Shouldly;

[TestFixture]
public class MaterialWithNameAndUsernameSpecificationTests : SpecificationFixture
{
    [Test]
    public void Evaluate_MaterialExists_Success()
    {
        // Arrange
        var material = materials.FirstOrDefault(x => x.UserName == FIRST_USERNAME);

        // Act
        var found = materials.FirstOrDefault(new MaterialWithNameAndUsernameSpecification(material.Name, FIRST_USERNAME).Criteria.Compile());

        // Assert
        found.ShouldBe(material);
    }

    [Test]
    public void Evaluate_MaterialDoesNotExists_Success()
    {
        // Arrange
        var material = materials.FirstOrDefault(x => x.UserName == FIRST_USERNAME);

        // Act
        var found = materials.FirstOrDefault(new MaterialWithNameAndUsernameSpecification(material.Name, NON_EXISTING_USERNAME).Criteria.Compile());

        // Assert
        found.ShouldBeNull();
    }
}
