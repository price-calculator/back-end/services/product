namespace Product.UnitTests.Application.Specifications;

using System.Linq;

using NUnit.Framework;

using Product.Application.Specifications;

using Shouldly;

[TestFixture]
public class ProductsWithMaterialIdSpecificationTests : SpecificationFixture
{
    [Test]
    public void Evaluate_ProductsExists_Success()
    {
        // Arrange
        var material = materials.FirstOrDefault(x => x.UserName == FIRST_USERNAME);

        // Act
        var productResults = products.Where(new ProductsWithMaterialIdSpecification(material.Id).Criteria.Compile()).ToList();

        // Assert
        var productsToCompare = products.Where(x => x.ProductMaterialLinks.Any(y => y.MaterialId == material.Id)).ToList();

        productResults.ShouldNotBeEmpty();
        productResults.Count.ShouldBe(productsToCompare.Count);
        foreach (var product in productResults)
        {
            productsToCompare.ShouldContain(product);
        }
    }

    [Test]
    public void Evaluate_ProductsDoesNotExists_Success()
    {
        // Act
        var productResults = products.Where(new ProductsWithMaterialIdSpecification(0).Criteria.Compile()).ToList();

        // Assert
        productResults.ShouldBeEmpty();
    }
}
